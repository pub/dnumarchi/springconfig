# Projet exemple de configuration d'une application Spring simple

## Contexte

Ce projet illustre la configuration d'une application Spring MVC non Spring Boot.

Il est volontairement simple et centré sur le sujet de la configuration.

## Pré-requis

Les pré-requis du projet :

* Une machine virtuelle Java de version 8 minimale.
* L'outil de construction Maven.

## Présentation du projet

### Description du projet

Ce projet contient un seul cas d'utilisation : afficher les propriétés `prenom` et `nom` définies dans la configuration.

## Construction du projet

Utiliser les commandes Maven usuelles.

* `mvn compile` : compilation du projet
* `mvn package` : création du livrable (war)
* _etc._

## Déploiement de l'application

Déployer l'artefact war produit sur un serveur Java tel Apache Tomcat ou ledémarrer en ligne de commande
par le plugin Maven Jetty embarqué dans le projet (exemples ci-après).

## Démonstration

### Configuration par défaut

La configuration par défaut se trouve dans le fichier `configuration/configuration-default.properties`.
Ce fichier se trouve dans l'application web sous WEB-INF/classes.

Démarrer l'application par Maven :

```shell
mvn jetty:run
```

Ouvrir un navigateur à l'adresse http://localhost:8080 pour accéder à l'application.

### Configuration locale externalisée

Pour surcharger la configuration par défaut dans un fichier externalisé :

1. Créer un fichier de type properties Java.
2. Renseigner dans ce ficher la propriété `nom` pour illuster la surcharge d'un seul paramètre.
3. Déclarer de ficher au démarrage par la propriété système `demo.configurationfile`

Un fichier exemple se trouve dans le dossier src/test/resources :

```shell
mvn jetty:run -Ddemo.configurationfile=./src/test/resources/local.properties
```

### Configuration par variable d'environnement

(Approche 12 facteurs, la plus compatible environnement cloud)

Les propriétés `prenom` et `nom` peuvent aussi être surchargée par les variables d'environnement
`DEMO_PRENOM` et `DEMO_NOM` respectivement.

```shell
export DEMO_NOM=Environnement
mvn jetty:run
```

~ Fin de document ~
