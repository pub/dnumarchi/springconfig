package demo.configuration.spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class HelloService {
    @Value("${prenom}")
    private String prenom;
    @Value("${nom}")
    private String nom;

    public String hello() {
        return String.format("Hello %s %s", prenom, nom);
    }
}