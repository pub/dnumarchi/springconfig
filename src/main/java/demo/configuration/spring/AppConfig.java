package demo.configuration.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@ComponentScan(basePackages = { "demo.configuration" })
@PropertySources({
	@PropertySource(value = "classpath:configuration/configuration-default.properties"),
	@PropertySource(value = "file:${demo.configurationfile}", ignoreResourceNotFound=true)
})
public class AppConfig {
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfig() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}
