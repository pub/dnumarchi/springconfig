package demo.configuration.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {
	@Autowired
	private HelloService hellosvc;

	@RequestMapping("/*")
	public ModelAndView hello() {
		return new ModelAndView("index", "hello", hellosvc.hello());
	}
}
